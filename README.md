# powershell template
[![Heimdall_Banner](https://winaero.com/blog/wp-content/uploads/2018/01/PowerShell-Logo-Banner.png)](https://docs.microsoft.com/en-us/powershell/)
___
## About
PowerShell template to help other getting started with a basic script.
___
## Useful links
Microsoft PowerShell site - https://docs.microsoft.com/en-us/powershell/<br>
Visit our website - https://luda-it.com<br>
___
